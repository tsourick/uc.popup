
uc = 
{
	getId: function ()
	{
		var f = arguments.callee;
		f.count = ++f.count || 0; 
		return 'uc-' + (f.count);
	},
	
	tpl: function (t)
	{
		var template = t;
		
		this.parse = function (vars)
		{
			var r = template;
			for (var k in vars)
			{
				var v = vars[k];
				
				var re = new RegExp('\\{' + k + '\\}', 'g');

				r = r.replace(re, v);
			}
			
			return r;
		}
	},
	
	isUrl: function (str)
	{
		var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
		// '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
		'(([a-z\\d]([a-z\\d-.])*)|'+ // domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
		'(\\#[-a-z\\d_]*)?$','i'); // fragment locator
		
		if (! pattern.test(str))
		{
			if (str.indexOf('file:///') === 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	},
	
	parseUrl: function (url)
	{
		var a = document.createElement('a');
		a.href = url;
		 
		return {
			protocol: a.protocol,
			hostname: a.hostname,
			port: a.port,
			pathname: a.pathname,
			search: a.search,
			hash: a.hash,
			host: a.host
		}		
	},
	
	applyValuesIf: function (o, overrides)
	{
		o = o || {};
		or = overrides || {};

		// check all defined options and set to default value if no value specified
		for (var k in or)
		{
			var v = or[k];
			
			o[k] = (o[k] !== undefined) ? o[k] : v;
		}
		
		return o;
	},
	
	offsetToBody: function (j)
	{
		var bodyOffset = document.body.getBoundingClientRect();
		var offset = j.offset();
		
		offset.left -= bodyOffset.left;
		
		return offset;
	},
	
	popup: function (o)
	{
		var domClass = 'uc-popup';
		var domPrefix = domClass + '-';
		
		var pTemplate = new uc.tpl('\
			<div id="{id}" class="' + domClass + ' {class}">\
				<div class="' + domPrefix + 'overlay" style="display: none"></div>\
				<div class="' + domPrefix + 'wrapper {autoCenter}" style="display: none">\
					\
					<div class="' + domPrefix + 'loading {autoCenter}" style="display: none"></div>\
					\
					<div class="' + domPrefix + 'box" style="display: none">\
						<div class="' + domPrefix + 'title"></div>\
						<div class="' + domPrefix + 'body">\
							<div class="' + domPrefix + 'content"></div>\
							<div class="' + domPrefix + 'buttons">{buttons}</div>\
						</div>\
						{controls}\
					</div>\
					\
				</div>\
			</div>\
		');
		/*
		var bTemplate = new uc.tpl('\
			<div class="' + domPrefix + 'button ' + domPrefix + '{name}">\
				<div class="uc-ieNegativeMarginFixer">\
					<div class="' + domPrefix + 'mid"></div>\
					<div class="' + domPrefix + 'left"></div>\
					<div class="' + domPrefix + 'right"></div>\
					<input type="button" value="{text}" />\
				</div>\
			</div>\
		');
		*/
		var bTemplate = new uc.tpl('\
			<div class="' + domPrefix + 'button ' + domPrefix + '{name}">\
					<input type="button" value="{text}" />\
			</div>\
		');

		var cTemplate = new uc.tpl('\
			<div class="' + domPrefix + 'control ' + domPrefix + '{name}"><div><span></span>{content}</div></div>\
		');
		
		this.id = uc.getId();
		this.jC = null;
		this.jButtons = {};
		this.jControls = {};
		this.handlers = []; // handler configs as: {f: <function>, [opt1, opt2, ...]}
		
		this.jWrapper = null;
		this.jBox = null;
		this.jOverlay = null;
		this.jLoading = null;
		this.jTitle = null;
		this.jBody = null;
		this.jContent = null;
		
		this.jAnchor = null;
		
		this.xhr = null;
		this.image = null;
		
		this.o = // defaults, may be overriden with options
		{
			type: 'autodetect',
			closeOnOverlay: true,
			destroyOnClose: false,
			
			parentNode: null,
			autoCenter: true,

			anchorNode: null,
			
			modal: true
		};
		
		var self = this;
		
		
		this.get = function (name)
		{
			return name ? self.jC.find('.' + domPrefix + name) : self.jC;
		}
		
		this.find = function (selector)
		{
			return self.jC.find(selector);
		}

		var fire = function (name, args)
		{
			// console.log(name);

			var r = true;
			

			for (var i = 0; i < self.handlers.length; i++)
			{
				var meta = self.handlers[i];
				

				if (meta.name == name)
				{
					args = args || [];
					args.unshift(self);
					r = meta.f.apply(null, args);
					
					if (meta.single)
					{
						self.handlers.splice(i, 1);
						i--;
					}
				}
			}

			
			return r;
		}
		
		/**
		* @param object handlers  object of handlers
		* @param object options   common options for all handlers
		*
		* Single handler can be passed as {f: <handler function>, single: (true|false)}
		* or as just a function.
		*/
		var setHandlers = function (handlers, options)
		{
			options = options || {};
			
			
			for (var n in handlers)
			{
				var meta = handlers[n];
				
				if ($.isFunction(meta))
				{
					meta = {f: meta};
				}
				
				meta = uc.applyValuesIf(meta, options);
				meta.name = n;

				self.handlers.push(meta);
			}
		}
		
		this.center = function ()
		{
			var jBox = self.jBox;
			
			var w = jBox.width();
			var h = jBox.height();
			
			jBox.css({'margin-left': 0 - Math.round(w / 2) + 'px'});
			jBox.css({'margin-top': 0 - Math.round(h / 2) + 'px'});
/*
			var jLoading = this.jC.find('.loading');
			var h = jLoading.height();
			jLoading.css({'margin-top': 0 - Math.round(h / 2) + 'px'});
			*/
		}

		this.setTitle = function (title)
		{
			self.jTitle.html(title);
		}
		
		var setContentAsJQuery = function ()
		{
		}
		var setContentAsHtml = function ()
		{
		}
		
		
		var isImage = function (path)
		{
			var re = new RegExp('\\.(jpe?g|png|gif|bmp)$', 'i');
			
			return re.test(path);
		}
		
		var detectContentType = function (c)
		{
			var type = null;
			
			if (c instanceof jQuery)
			{
				type = 'jquery';
			}
			else if (uc.isUrl(c))
			{
				var urlParts = uc.parseUrl(c);
				
				if (isImage(urlParts.pathname))
				{
					type = 'image';
				}
				else
				{
					type = 'url';
				}
			}
			
			return type;
		}
		
		var stopLoading = function ()
		{
			if (self.image)
			{
				$(self.image).removeAttr('src');
			}
			
			if (self.xhr)
			{
				self.xhr.abort();
			}
		}
		
		this.setContent = function (content, type, callback)
		{
			type = (type == 'autodetect') ? detectContentType(content) : type;
			
			var jC = self.jContent;
			switch (type)
			{
				case 'jquery':
					jC.append(content);
					
					callback();
				break;
				
				case 'image':
					self.image = self.image || new Image();
					
					var i = self.image;
					i.onload = function ()
					{
						jC.append(i);
						// self.middle();
					
						hideLoading();
						
						callback(self);
					}
					
					showLoading();
					
					i.src = content;
				break;
				
				case 'url':
					showLoading();
					
					self.xhr = $.ajax
					(
						{
							// async: false,
							url: content,
							dataType: 'html',
							success: function (data, status, xhr)
							{
								jC.html(data);
								// callback();
							},
							error: function (xhr, status, error)
							{
							},
							complete: function (xhr, status)
							{
								hideLoading();
								
								callback(self);
							}
						}
					);
				break;
				
				default:
					jC.html(content);
					
					callback(self);
				break;
			}
			
			
			// if (! dontMiddle) this.middle();
		}
		
		var registerButton = function (name, handler)
		{
			var jBtn = self.jC.find('.' + domPrefix + 'button.' + domPrefix + name);

			self.jButtons[name] = jBtn;

			
			if (handler)
			{
				jBtn.click
				(
					function ()
					{
						fire('button', [name]);
					}
				);
			}
			// hideButton(name);
		}

		var registerControl = function (name, handler)
		{
			var jCtrl = self.jC.find('.' + domPrefix + 'control.' + domPrefix + name);

			self.jControls[name] = jCtrl;

			
			if (handler)
			{
				jCtrl.click
				(
					function ()
					{
						fire('control', [name]);
					}
				);
			}
			// hideButton(name);
		}
		
		var showButton = function (name, handler)
		{
			var jBtn = self.jButtons[name];
			
			jBtn.css({'display': 'inline-block'});
			
			
			if (handler)
			{
				jBtn.click
				(
					function ()
					{
						fire('button', [name]);
					}
				);
			}
		}

		var hideButton = function (name)
		{
			var jBtn = self.jButtons[name];
			
			jBtn.css({'display': 'none'});

			jBtn.off('click');
		}
		
		
		var removeInternalHandlers = function ()
		{
			for (var k in self.jButtons)
			{
				var jButton = self.jButtons[k];
				jButton.off();
			}
			
			self.jBox.off();

			self.jWrapper.off();
			self.jOverlay.off();
		}
		
/*
		var setPosition = function (o)
		{
			if (o.top)
			{
				self.jWrapper.css('top', o.top);
			}

			if (o.left)
			{
				self.jWrapper.css('left', o.left);
			}
		}
*/
		var setPosition = function (o)
		{
			if (o.anchorNode)
			{
				var jAnchor = $(o.anchorNode);
				
				var jCParent = self.jC.offsetParent();
				
				var jAParent = jAnchor.offsetParent();
				
				
				var top, left;
				
				/*
				if (jCParent.get(0) === jAParent.get(0)) // anchor's position refers to the same node as popup's position
				{
					top = jAnchor.position().top;
					left = jAnchor.position().left;
				}
				else if ($.contains(jCParent.get(0), jAParent.get(0))) // popup's reference parent contains anchor's reference parent
				{
					var cpOffset = (jCParent.get(0) === $('body').get(0)) ? {left: 0, top: 0} : jCParent.offset();
					var aOffset = (jAnchor.get(0) === $('body').get(0)) ? {left: 0, top: 0} : jAnchor.offset();
	
					top = aOffset.top - cpOffset.top;
					left = aOffset.left - cpOffset.left;
				}
				else
				{
					throw 'unexpected stub: anchor node is outside popup\'s parent reference container';
				}
				*/
				if
				(
					jCParent.get(0) === jAParent.get(0) // anchor's position refers to the same node as popup's position
					|| $.contains(jCParent.get(0), jAParent.get(0)) // popup's reference parent contains anchor's reference parent
				)
				{
					var cpOffset = (jCParent.get(0) === $('body').get(0)) ? {left: 0, top: 0} : uc.offsetToBody(jCParent);
					var aOffset = (jAnchor.get(0) === $('body').get(0)) ? {left: 0, top: 0} : uc.offsetToBody(jAnchor);
	
					top = aOffset.top - cpOffset.top;
					left = aOffset.left - cpOffset.left;
				}
				else
				{
					throw 'unexpected stub: anchor node is outside popup\'s parent reference container';
				}
				
				
				if (o.top)
				{
					top += o.top;
				}
		
				if (o.left)
				{
					left += o.left;
				}
				
				self.jWrapper.css('top', top);
				self.jWrapper.css('left', left);
			}
			else
			{
				if (o.top)
				{
					self.jWrapper.css('top', o.top);
				}
		
				if (o.left)
				{
					self.jWrapper.css('left', o.left);
				}
			}
		}

		var setSize = function (o)
		{
			if (o.width)
			{
				self.jBox.width(o.width);
			}

			if (o.height)
			{
				self.jBox.height(o.height);
			}

			if (o.contentWidth)
			{
				self.jContent.width(o.contentWidth);
			}
			
			if (o.contentHeight)
			{
				self.jContent.height(o.contentHeight);
			}

			if (o.minContentWidth)
			{
				self.jContent.css('min-width', o.minContentWidth);
			}
			
			if (o.minContentHeight)
			{
				self.jContent.css('min-height', o.minContentHeight);
			}
		}
		

		var getOption = function (name)
		{
		}
		
		var getOptions = function (o)
		{
			o = o || {};
			
			// params
			o.title = (o.title !== undefined) ? o.title : null;
			o.content = (o.content !== undefined) ? o.content : null;
			// o.buttons = o.buttons || [];
			// o.controls = o.controls || [];
			o.handlers = o.handlers || {};

			o['class'] = o['class'] || '';
			
			
			// options
			o.type = (o.type !== undefined) ? o.type : self.o.type;
			o.closeOnOverlay = (o.closeOnOverlay !== undefined) ? o.closeOnOverlay : self.o.closeOnOverlay;
			o.destroyOnClose = (o.destroyOnClose !== undefined) ? o.destroyOnClose : self.o.destroyOnClose;
			o.parentNode = (o.parentNode !== undefined) ? o.parentNode : self.o.parentNode;
			o.autoCenter = (o.autoCenter !== undefined) ? o.autoCenter : self.o.autoCenter;
			o.anchorNode = (o.anchorNode !== undefined) ? o.anchorNode : self.o.anchorNode;
			o.modal = (o.modal !== undefined) ? o.modal : self.o.modal;
			o.left = (o.left !== undefined) ? o.left : self.o.left;
			o.top = (o.top !== undefined) ? o.top : self.o.top;

			if (o.anchorNode) o.autoCenter = false;

			
			// save options
			self.o.type = o.type;
			self.o.closeOnOverlay = o.closeOnOverlay;
			self.o.destroyOnClose = o.destroyOnClose;
			self.o.parentNode = o.parentNode;
			self.o.autoCenter = o.autoCenter;
			self.o.anchorNode = o.anchorNode;
			self.o.modal = o.modal;
			self.o.left = o.left;
			self.o.top = o.top;

			
			return o;
		}
		
		
		var showWrapper = function ()
		{
			self.jWrapper.show();
		}
		var hideWrapper = function ()
		{
			self.jWrapper.hide();
		}
		var showBox = function ()
		{
			self.jBox.css('display', 'inline-block');
		}
		var hideBox = function ()
		{
			self.jBox.hide();
		}
		var showOverlay = function ()
		{
			self.jOverlay.show();
		}
		var hideOverlay = function ()
		{
			self.jOverlay.hide();
		}
		var showLoading = function ()
		{
			self.jLoading.css('display', 'inline-block');
		}
		var hideLoading = function ()
		{
			self.jLoading.hide();
		}
		
		var beforeShow = function ()
		{
			var jButtons = self.get('buttons');
			
			if (jButtons.children().length > 0)
			{
				jButtons.removeClass(domPrefix + 'empty');
			}
			else
			{
				jButtons.addClass(domPrefix + 'empty');
			}
		}

		this.show = function (o)
		{
			o = getOptions(o);
			
			// this.o.destroyOnClose = o.destroyOnClose;
			
			setHandlers(o.handlers);
			
			if (fire('beforeShow') !== false)
			{
				beforeShow();
				
				
				if (o.modal) showOverlay();
				showWrapper();
				
				if (o.title != null)
				{
					self.setTitle(o.title);
				}
				
				var afterSetContent = function ()
				{
					setPosition(o);
					
					setSize(o);
					
					fire('afterSetContent');
					
					// self.jC.show();
					showBox();
					if (o.autoCenter) self.center();
		
					fire('afterShow');
				}
				
				if (o.content != null)
				{
					self.setContent(o.content, o.type, afterSetContent);
				}
				else
				{
					afterSetContent();
				}
			}
		}
		
		this.hide = function ()
		{
			if (fire('beforeHide') !== false)
			{
				stopLoading();

				// this.jC.hide();
				hideBox();
				hideWrapper();
				hideOverlay();
				
				hideLoading();
				/*
				for (var k in that.jButtons)
				{
					hideButton(k);
				}
				
				removeInternalHandlers();
				*/
				fire('afterHide');
				
				
				if (this.o.destroyOnClose)
				{
					this._destroy();
				}
			}
		}
		
		this.empty = function ()
		{
			self.jContent.empty();
		}
		
		this._destroy = function ()
		{
			removeInternalHandlers();
			
			this.jC.get(0).ucPopup = null;
			this.jC.remove();
			this.jC = null;
			this.jButtons = null;
		}
		
		this.destroy = function ()
		{
			if (fire('beforeDestroy') !== false)
			{
				// this.hide();

				this._destroy();
	
				// this.handlers = null;
				// fire('afterDestroy');
			}
		}
		
		// common options: title, content, type, destroyOnClose, closeOnOverlay, top, left, contentWidth, contentHeight, minContentWidth, minContentHeight
		// init options: buttons, controls, handlers`persistent handlers`, [parentNode, anchorNode, buttonTemplate, controlTemplate, template, autoCenter, <common options>]
		// show options: [handlers`once fired handlers` , <common options>]
		
		
		
		var createButtonsHTML = function (cs)
		{
			var html = '';
			
			for (var i = 0; i < cs.length; i++)
			{
				var name = cs[i];
				var text = cs[i];
				
				html += bTemplate.parse({name: name, text: text});
			}
			
			return html;
		}
		
		var createControlsHTML = function (cs)
		{
			var html = '';
			
			for (var i = 0; i < cs.length; i++)
			{
				var c = cs[i];
				
				if (typeof c == 'string')
				{
					html += cTemplate.parse({name: c, content: c});
				}
				else // assume object
				{
					var n = c.name;
					
					if (c.template) // custom template if any
					{
						var t = new uc.tpl(c.template);
					}
					else
					{
						var t = cTemplate;
					}
					
					html += t.parse({name: n, content: n});
				}
			}
			
			return html;
		}

		var init = function (o)
		{
			// Prepare buttons' html
			
			var buttonConfigs = o.buttons || [];

			var _buttons = createButtonsHTML(buttonConfigs);


			// Prepare controls' html
			
			var controlConfigs = o.controls || [];

			var _controls = createControlsHTML(controlConfigs);


			o = getOptions(o);
			
			// self.o.destroyOnClose = o.destroyOnClose;

			
			var jRoot; //, locationClass;
			if (o.parentNode)
			{
				jRoot = $(o.parentNode);
				// locationClass = '';
			}
			else
			{
				jRoot = $('body');
				// locationClass = domPrefix + 'fixed';
			}
			
			var centerClass = '';
			if (o.autoCenter)
			{
				centerClass = domPrefix + 'autoCenter';
				
				var autoCenterMode; // TODO: different modes
				switch (autoCenterMode)
				{
					default:
						centerClass += ' ' + domPrefix + 'fixed';
					break;
				}
			}
			
			
			// Append popup's html to DOM
			
			var t = o.template || pTemplate;
			// var _html = t.parse({id: self.id, buttons: _buttons, controls: _controls, location: locationClass, autoCenter: centerClass});
			var _html = t.parse({id: self.id, 'class': o['class'], buttons: _buttons, controls: _controls, autoCenter: centerClass});
			
			jRoot.append(_html);
			
			
			// Store elements
			
			self.jC = $('#' + self.id);
			
			self.jWrapper = self.get('wrapper');
			self.jBox = self.get('box');
			self.jOverlay = self.get('overlay');
			self.jLoading = self.get('loading');
			self.jTitle = self.get('title');
			self.jBody = self.get('body');
			self.jContent = self.get('content');
			
			
			// Set backlink from container DOM node
			self.jC.get(0).ucPopup = self;
			

			// Set handlers
			
			setHandlers(o.handlers);
			
			
			// Set title
			
			if (o.title)
			{
				self.setTitle(o.title);
			}
			
			
			// Set content
			
			var afterSetContent = function ()
			{
				setPosition(o);
				
				setSize(o);
				
				if (o.closeOnOverlay)
				{
					self.jBox.click(function (e)
					{
						e.stopPropagation();
					});
					self.jLoading.click(function (e)
					{
						e.stopPropagation();
					});
	
					self.jWrapper.click(function ()
					{
						self.hide();
					});
					self.jOverlay.click(function ()
					{
						self.hide();
					});
				}
	
	
				var onButton = o.handlers.button || function () {};
				for (var i = 0; i < buttonConfigs.length; i++)
				{
					var name = buttonConfigs[i];
					
					registerButton(name, onButton);
				}
	
				var onControl = o.handlers.control || function () {};
				for (var i = 0; i < controlConfigs.length; i++)
				{
					var c = controlConfigs[i];
				
					if (typeof c == 'string')
					{
						registerControl(c, onControl);
					}
					else
					{
						registerControl(c.name, onControl);
					}
				}
			}

			if (o.content)
			{
				self.setContent(o.content, o.type, afterSetContent);
			}
			else
			{
				afterSetContent();
			}
		}
		
		init(o);
	},
	
	alert: function (title, content, handler, o)
	{
		o = o || {};
		o.title = title;
		o.content = content;
		o.buttons = ['ok'];
		o.handlers =
		{
			button: function (p, name)
			{
				// p.destroy();
				p.hide();
				
				if (handler) handler();
			}
		};
		o.closeOnOverlay = true;
		o.destroyOnClose = true;
		
		(new uc.popup(o)).show();
	}
};

