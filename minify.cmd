
@echo off

echo.
echo Compressing JS
type _header > uc.popup.js
call yuic --type js --charset utf-8 src.uc.popup.js >> uc.popup.js

echo.
echo Compressing CSS
type _header > uc.popup.css
call yuic --type css --charset utf-8 src.uc.popup.css >> uc.popup.css

echo.
pause
